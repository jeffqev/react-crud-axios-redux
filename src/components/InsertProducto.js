import React, { useState } from 'react'
// import PropTypes from 'prop-types'
import { crearNuevoProductoAction } from '../action/productoAction'
import { useDispatch, useSelector } from 'react-redux'


const InsertProducto = ({ history }) => {

    // State del componente
    const [nombre, setNombre] = useState('');
    const [precio, setPrecio] = useState(0);

    // Redux ejecutar action
    const dispatch = useDispatch()
    const agregarProducto = producto => dispatch(crearNuevoProductoAction(producto))

    // acceder al state del store 
    const cargado = useSelector(state => state.productos.loading)
    const error = useSelector(state => state.productos.error)



    //Validar e insertar
    const nuevoProducto = e => {
        e.preventDefault();
        // valir
        if (nombre.trim() === '' || precio <= 0) {
            return;
        }

        // errores

        // crearnuevo
        agregarProducto({
            nombre,
            precio
        });

        //go home
        history.push('/');
    }
    return (
        <div className="row justify-content-center ">
            <div className="col-md-8">

                <div className="card">

                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Agregar producto
                      </h2>
                        <form
                            onSubmit={nuevoProducto}
                        >
                            <div className="form-group">
                                <label htmlFor="nombre"> Nombre producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre del producto"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => setNombre(e.target.value)}
                                />

                                <label htmlFor=""> Precio producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio del producto"
                                    name="precio"
                                    value={precio}
                                    onChange={e => setPrecio(Number(e.target.value))}
                                />

                                <button
                                    type="submit"
                                    className="mt-3 btn btn-primary font-weight-bold text-uppercase d-block w-100"
                                >
                                    Agregar
                            </button>
                            </div>
                        </form>
                        {cargado ? <p>CARGANDO</p> : null}
                        {error ? <p className="alert alert-danger p2 mt-4 text-center" >Error</p> : null}
                    </div>
                </div>

            </div>

        </div>
    )
}

// InsertProducto.propTypes = {

// }

export default InsertProducto

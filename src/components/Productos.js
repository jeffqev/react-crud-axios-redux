import React, { Fragment, useEffect } from 'react'
// import PropTypes from 'prop-types'

//Redux 
import { getProductosAction } from '../action/productoAction'
import { useDispatch, useSelector } from 'react-redux'
import { Produto } from './Produto';

const Productos = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        const cargarProductos = () => dispatch(getProductosAction());

        cargarProductos()

    }, [dispatch])

    //Obtener el state
    const productos = useSelector(state => state.productos.productos)
    const error = useSelector(state => state.productos.error)



    return (
        <Fragment>
            <h2 className="text-center my-5" > Listado de productos</h2>
            {error ? <p className="font-weight-bold alert alert-danger text-center">Hubo un error</p> : null}
            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th scope="col" >Nombre</th>
                        <th scope="col" >Precio</th>
                        <th scope="col" >Accion</th>
                    </tr>
                </thead>
                <tbody>
                    {productos.length === 0 ? 'No se encontro data' : (
                        productos.map(producto => (
                            <Produto
                                key={producto.id}
                                producto={producto}
                            />
                        ))
                    )}
                </tbody>
            </table>

        </Fragment>
    )
}

// Productos.propTypes = {

// }

export default Productos

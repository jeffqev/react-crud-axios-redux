import React from "react";
import { Link } from "react-router-dom";
// import PropTypes from 'prop-types'

const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary justify-content-between ">
      <div className="container">
        <h1>
          <Link to={"/"} className="text-light">
            CRUD
          </Link>
        </h1>
      </div>

      <Link
        className="btn btn-danger nuevo-post d-block d-md-inline-block"
        to={"/productos/nuevo"}
      >
        Agregar producto &#43;
      </Link>
    </nav>
  );
};

// Header.propTypes = {

// }

export default Header;

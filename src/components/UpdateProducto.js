import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { EditarProductosAction } from '../action/productoAction';

const UpdateProducto = () => {

    const [form, setForm] = useState({
        nombre: '',
        precio: '',
        id: ''
    })

    const producto = useSelector(state => state.productos.editar);

    const history = useHistory();
    const dispatch = useDispatch();
    // if (!producto) {
    //     history.push(`/`);
    //     return null
    // };

    useEffect(() => {
        setForm(producto)
    }, [producto])

    const { nombre, precio } = form;




    const actualizarInput = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const updateProducto = e => {
        e.preventDefault();

        dispatch(EditarProductosAction(form));
        history.push(`/`);
    }

    return (

        <div className="row justify-content-center ">
            <div className="col-md-8">

                <div className="card">

                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Editar producto
                      </h2>
                        <form
                            onSubmit={updateProducto}
                        >
                            <div className="form-group">
                                <label htmlFor="nombre"> Nombre producto</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre del producto"
                                    name="nombre"
                                    //value={producto ? producto.nombre : history.push(`/`)}
                                    value={nombre}
                                    onChange={actualizarInput}
                                />

                                <label htmlFor=""> Precio producto</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Precio del producto"
                                    name="precio"
                                    value={precio}
                                    onChange={actualizarInput}
                                />

                                <button
                                    type="submit"
                                    className="mt-3 btn btn-primary font-weight-bold text-uppercase d-block w-100"
                                >
                                    Guardar cambios
                            </button>
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    )
}

export default UpdateProducto

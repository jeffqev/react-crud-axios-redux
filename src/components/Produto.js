import React from 'react'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
// Redux
import { useDispatch } from 'react-redux';
import { eliminarProductosAction, updateGetProductosAction } from '../action/productoAction';



export const Produto = ({ producto }) => {
    const dispatch = useDispatch();
    const history = useHistory();
    // Eliminar
    const eliminarProducto = id => {
        //confirmar eliminacion
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                dispatch(eliminarProductosAction(id));
            }
        })
        // enviar la accion

    }
    // redirigir de forma programada
    const redireccionar = producto => {
        dispatch(updateGetProductosAction(producto));
        history.push(`/productos/editar/${producto.id}`)
    }
    return (
        <tr>

            <td>{producto.nombre}</td>
            <td> <span className="font-weight-bold">{producto.precio}</span></td>
            <td className="acciones">
                <button
                    type="button"
                    className="btn btn-primary mr-2"
                    onClick={() => redireccionar(producto)}
                >Editar</button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => eliminarProducto(producto.id)}
                >Eliminar</button>
            </td>
        </tr>
    )
}

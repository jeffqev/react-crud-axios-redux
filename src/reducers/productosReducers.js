import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_OK,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_OK,
    DESCARGA_PRODUCTOS_ERROR,
    INICIAR_ELIMINACION,
    INICIAR_ELIMINACION_OK,
    INICIAR_ELIMINACION_ERROR,
    OBTENER_PRODUCTO_UPDATE,
    INICIAR_UPDATE,
    INICIAR_UPDATE_OK,
    INICIAR_UPDATE_ERROR
} from '../types'

const inicialState = {
    productos: [],
    error: null,
    loading: false,
    eliminar: null,
    editar: null
}

export default function (state = inicialState, action) {
    switch (action.type) {
        case AGREGAR_PRODUCTO:
            return {
                ...state,
                loading: true
            }
        case AGREGAR_PRODUCTO_OK:
            return {
                ...state,
                loading: false,
                productos: [...state.productos, action.payload]
            }
        case DESCARGA_PRODUCTOS_ERROR:
        case AGREGAR_PRODUCTO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case COMENZAR_DESCARGA_PRODUCTOS:
            return {
                ...state,
                loading: action.payload

            }
        case DESCARGA_PRODUCTOS_OK:
            return {
                ...state,
                loading: false,
                error: null,
                editar: null,
                productos: action.payload

            }
        case INICIAR_ELIMINACION:
            return {
                ...state,
                eliminar: action.payload
            }

        case INICIAR_ELIMINACION_OK:
            return {
                ...state,
                productos: state.productos.filter(producto => producto.id !== state.eliminar),
                eliminar: null,
                error: action.payload

            }
        case INICIAR_ELIMINACION_ERROR:
            return {
                ...state,
                error: action.payload,
                eliminar: null,

            }
        case OBTENER_PRODUCTO_UPDATE:
            return {
                ...state,
                error: false,
                editar: action.payload,

            }
        case INICIAR_UPDATE:
            return {
                ...state,
                loading: true,


            }
        case INICIAR_UPDATE_OK:
            return {
                ...state,
                error: false,
                editar: null,
                loading: false,
                productos: state.productos.map(producto =>
                    producto.id === action.payload.id ? producto = action.payload : producto
                ),

            }
        case INICIAR_UPDATE_ERROR:
            return {
                ...state,
                error: action.payload,
            }
        default:
            return state;

    }
}
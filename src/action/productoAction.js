import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_OK,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_OK,
    DESCARGA_PRODUCTOS_ERROR,
    INICIAR_ELIMINACION,
    INICIAR_ELIMINACION_OK,
    INICIAR_ELIMINACION_ERROR,
    OBTENER_PRODUCTO_UPDATE,
    INICIAR_UPDATE,
    INICIAR_UPDATE_OK,
    INICIAR_UPDATE_ERROR
} from '../types'
import clienteAxios from '../config/axios'
import Swal from 'sweetalert2'

// INSERTAR PRODUCTO
export function crearNuevoProductoAction(producto) {
    return async (dispatch) => {
        dispatch(agregarProducto());

        try {
            await clienteAxios.post('/productos', producto)
            dispatch(agregarProductoOk(producto));
            Swal.fire(
                'Correcto',
                'Producto ingresado con exito',
                'success'
            )
        } catch (error) {
            console.log(error);
            dispatch(agregarProductoError(true));
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Intentalo de nuevo'
            })
        }

    }

}

const agregarProducto = () => ({
    type: AGREGAR_PRODUCTO
})

const agregarProductoOk = producto => ({
    type: AGREGAR_PRODUCTO_OK,
    payload: producto

})

const agregarProductoError = (error) => ({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: error
})


// MOSTRAR PRODUCTOS
export function getProductosAction() {
    return async (dispatch) => {
        dispatch(descargarProdcutos())

        try {
            const respuesta = await clienteAxios.get('/productos')
            dispatch(descargarProdcutosOK(respuesta.data))
        } catch (error) {

            console.log(error);
            dispatch(descargarProdcutosError())
        }
    }
}

const descargarProdcutos = () => ({
    type: COMENZAR_DESCARGA_PRODUCTOS,
    payload: true
})

const descargarProdcutosOK = (productos) => ({
    type: DESCARGA_PRODUCTOS_OK,
    payload: productos
})

const descargarProdcutosError = () => ({
    type: DESCARGA_PRODUCTOS_ERROR,
    payload: true
})


// ELIMINAR PRODUCTO

export function eliminarProductosAction(id) {
    return async (dispatch) => {
        dispatch(eliminarProdcutos(id))

        console.log(id);


        try {
            await clienteAxios.delete(`/productos/${id}`)
            dispatch(eliminarProdcutosOk())
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        } catch (error) {

            console.log(error);
            dispatch(eliminarProdcutosError())
            Swal.fire({
                icon: 'error',
                title: 'Error al eliminar',
                text: 'Intentalo un unos minutos de nuevo nuevo'
            })
        }
    }
}

const eliminarProdcutos = (id) => ({
    type: INICIAR_ELIMINACION,
    payload: id
})

const eliminarProdcutosOk = () => ({
    type: INICIAR_ELIMINACION_OK,
    payload: false
})

const eliminarProdcutosError = () => ({
    type: INICIAR_ELIMINACION_ERROR,
    payload: true
})

// EDITAR PRODUCTO

// OBTENER EL PRODUCTO A EDITAR
export function updateGetProductosAction(producto) {
    return async (dispatch) => {
        dispatch(getProdcutos(producto))

    }
}
const getProdcutos = (producto) => ({
    type: OBTENER_PRODUCTO_UPDATE,
    payload: producto
})

// INICIAR EDICION

export function EditarProductosAction(producto) {
    return async (dispatch) => {
        dispatch(editarProdcutos())

        try {
            await clienteAxios.put(`/productos/${producto.id}`, producto)
            dispatch(editarProdcutosOk(producto))

        } catch (error) {
            console.log(error);
            dispatch(editarProdcutosError())
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Intentalo de nuevo'
            })
        }

    }
}

const editarProdcutos = () => ({
    type: INICIAR_UPDATE
})

const editarProdcutosOk = (newproducto) => ({
    type: INICIAR_UPDATE_OK,
    payload: newproducto
})

const editarProdcutosError = () => ({
    type: INICIAR_UPDATE_ERROR,
    payload: true
})

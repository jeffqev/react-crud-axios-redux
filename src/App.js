import React from 'react';
import Header from './components/Header';
import Productos from './components/Productos';
import InsertProducto from './components/InsertProducto';
import UpdateProducto from './components/UpdateProducto';

//React router
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

//Redux

import { Provider } from 'react-redux'
import store from './store'


function App() {
  return (
    <Router>
      <Provider store={store}>
        <Header />

        <div className="container mt-5">
          <Switch>
            <Route exact path="/" component={Productos} />
            <Route exact path="/productos/nuevo" component={InsertProducto} />
            <Route exact path="/productos/editar/:id" component={UpdateProducto} />

          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default App;
